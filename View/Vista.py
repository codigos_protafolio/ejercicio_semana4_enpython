from Model.Vo.Lider import Lider
from Model.Vo.Proyecto import Proyecto
from Controller.Controlador import Controlador

class Vista:

    def __init__(self):
        self.controlador: Controlador = Controlador()

    def vista_requerimiento_1(self):
        proyectos: list = list()
        proyectos = self.controlador.Solucionar_requerimiento_1()
        for proyecto in proyectos:
            print("Constructora: "+proyecto.getNombre_constructora()+" - Serial: "+proyecto.getSerial())

    def vista_requerimiento_2(self):
        proyectos: list = list()
        proyectos = self.controlador.Solucionar_requerimiento_2()
        for proyecto in proyectos:
            print("Numero_Habitaciones: "+str(int(proyecto.getNum_habitaciones()))+" - Numero_Banos: "+str(int(proyecto.getNum_banios()))+" - Nombre_Lider: "+proyecto.getLider().getNombre()+" - Apellido_Lider: "+proyecto.getLider().getApellido()+" - Estrato_Proyecto: "+str(proyecto.getEstrato_proyecto()))
    
    def vista_requerimiento_3(self):
        proyectos: list = list()
        proyectos = self.controlador.Solucionar_requerimiento_3()
        for proyecto in proyectos:
            print("Cantidad_Casas: "+str(proyecto.getNum_casas())+" - Constructora: "+proyecto.getNombre_constructora())

    def vista_requerimiento_4(self):
        lideres: list = list()
        lideres = self.controlador.Solucionar_requerimiento_4()
        for lider in lideres:
            print("Nombre_Lider: "+lider.getNombre()+" - Apellido_Lider: "+lider.getApellido())

    def vista_requerimiento_5(self):
        proyectos: list = list()
        proyectos = self.controlador.Solucionar_requerimiento_5()
        for proyecto in proyectos:
            print("Cantidad_Casas: "+str(proyecto.getNum_casas())+" - Constructora: "+proyecto.getNombre_constructora())