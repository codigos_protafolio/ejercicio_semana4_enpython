from Model.Vo.Lider import Lider
from Model.Vo.Proyecto import Proyecto
from Model.Dao.LiderDao import LiderDao
from Model.Dao.ProyectoDao import ProyectoDao

class Controlador:

    def __init__(self):
        self.proyectoDao: ProyectoDao = ProyectoDao()
        self.liderDao: LiderDao = LiderDao()

    def Solucionar_requerimiento_1(self):
        return self.proyectoDao.query_requerimiento_1()

    def Solucionar_requerimiento_2(self):
        return self.proyectoDao.query_requerimiento_2()

    def Solucionar_requerimiento_3(self):
        return self.proyectoDao.query_requerimiento_3()

    def Solucionar_requerimiento_4(self):
        return self.liderDao.query_requerimiento_4()

    def Solucionar_requerimiento_5(self):
        return self.proyectoDao.query_requerimiento_5()