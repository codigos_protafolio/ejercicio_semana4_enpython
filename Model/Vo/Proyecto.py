from Model.Vo.Lider import Lider

class Proyecto:

    def __init__(self):
        self.fecha_inicio: str = ""
        self.num_habitaciones: int = 0
        self.num_banios: int = 0
        self.nombre_constructora: str = ""
        self.estrato_proyecto: int = 0
        self.serial: str = ""
        self.num_casas: int = 0
        self.lider: Lider = Lider("","")

    #Métodos consultores
    def getFecha_inicio(self):
        return self.fecha_inicio

    def setFecha_inicio(self, fecha_inicio: str):
        self.fecha_inicio = fecha_inicio

    def getNum_habitaciones(self):
        return self.num_habitaciones

    def setNum_habitaciones(self, num_habitaciones: int):
        self.num_habitaciones = num_habitaciones

    def getNum_banios(self):
        return self.num_banios

    def setNum_banios(self,num_banios: int):
        self.num_banios = num_banios

    def getNombre_constructora(self):
        return self.nombre_constructora

    def setNombre_constructora(self, nombre_constructora: str):
        self.nombre_constructora = nombre_constructora

    def getEstrato_proyecto(self):
        return self.estrato_proyecto

    def setEstrato_proyecto(self, estrato_proyecto: int):
        self.estrato_proyecto = estrato_proyecto

    def getSerial(self):
        return self.serial

    def setSerial(self, serial: str):
        self.serial = serial

    def getNum_casas(self):
        return self.num_casas

    def setNum_casas(self, num_casas: int):
        self.num_casas = num_casas

    def getLider(self):
        return self.lider

    def setLider(self, nombre: str, apellido: str):
        self.lider = Lider(nombre, apellido)
