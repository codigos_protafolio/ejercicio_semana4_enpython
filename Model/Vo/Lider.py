
class Lider:

    def __init__(self, nombre: str, apellido: str):
        self.nombre: str = nombre
        self.apellido: str = apellido

    #Métodos consultores

    def getNombre(self):
        return self.nombre

    def setNombre(self, nombre):
        self.nombre = nombre

    def getApellido(self):
        return self.apellido

    def setApellido(self, apellido):
        self.apellido = apellido
