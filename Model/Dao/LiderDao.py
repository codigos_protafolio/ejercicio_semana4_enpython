from Model.Vo.Lider import Lider
from Util.JDBCUtilities import JDBCUtilities

class LiderDao:

    def query_requerimiento_4(self):
        arrayLideres: list = list()
        query: str = "SELECT L.Nombre, L.Primer_Apellido "
        query += "FROM Proyecto P "
        query += "INNER JOIN Lider L ON L.ID_lider = P.ID_Lider "
        query += "WHERE P.Clasificacion = 'Casa'"
        jdbcutil = JDBCUtilities()
        conexion = jdbcutil.getConnection()
        cursor = conexion.execute(query)
        for fila in cursor:
            objLider: Lider = Lider(fila[0], fila[1])
            objLider.setNombre(fila[0])
            objLider.setApellido(fila[1])
            arrayLideres.append(objLider)
        conexion.close()
        return arrayLideres
