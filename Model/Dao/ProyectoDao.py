from Model.Vo.Lider import Lider
from Model.Vo.Proyecto import Proyecto
from Util.JDBCUtilities import JDBCUtilities

class ProyectoDao:

    def query_requerimiento_1(self):
        arrayProyectos: list = list()
        query: str = "SELECT Constructora, Serial "
        query += "FROM Proyecto "
        query += "WHERE Clasificacion = 'Casa'"
        jdbcutil = JDBCUtilities()
        conexion = jdbcutil.getConnection()
        cursor = conexion.execute(query)
        for fila in cursor:
            objProyecto: Proyecto = Proyecto()
            objProyecto.setNombre_constructora(fila[0])
            objProyecto.setSerial(fila[1])
            arrayProyectos.append(objProyecto)
        conexion.close()
        return arrayProyectos

    def query_requerimiento_2(self):
        arrayProyectos: list = list()
        query: str = "SELECT P.Numero_Habitaciones, P.Numero_Banos, L.Nombre, L.Primer_Apellido, T.Estrato "
        query += "FROM Proyecto P "
        query += "INNER JOIN Lider L ON L.ID_lider = P.ID_Lider ";
        query += "INNER JOIN Tipo T ON T.ID_Tipo = P.ID_Tipo ";
        query += "WHERE P.Clasificacion = 'Casa'"
        jdbcutil = JDBCUtilities()
        conexion = jdbcutil.getConnection()
        cursor = conexion.execute(query)
        for fila in cursor:
            objProyecto: Proyecto = Proyecto()
            objProyecto.setNum_habitaciones(fila[0])
            objProyecto.setNum_banios(fila[1])
            objProyecto.setEstrato_proyecto(fila[4])
            objProyecto.setLider(fila[2], fila[3])
            arrayProyectos.append(objProyecto)
        conexion.close()
        return arrayProyectos

    def query_requerimiento_3(self):
        arrayProyectos: list = list()
        query: str = "SELECT count(Constructora) as Casas, Constructora "
        query += "FROM Proyecto "
        query += "WHERE Clasificacion = 'Casa' "
        query += "GROUP BY Constructora"
        jdbcutil = JDBCUtilities()
        conexion = jdbcutil.getConnection()
        cursor = conexion.execute(query)
        for fila in cursor:
            objProyecto: Proyecto = Proyecto()
            objProyecto.setNum_casas(fila[0])
            objProyecto.setNombre_constructora(fila[1])
            arrayProyectos.append(objProyecto)
        conexion.close()
        return arrayProyectos

    def query_requerimiento_5(self):
        arrayProyectos: list = list()
        query: str = "SELECT count(Constructora) as Casas, Constructora "
        query += "FROM Proyecto "
        query += "WHERE Clasificacion = 'Casa' "
        query += "GROUP BY Constructora ";
        query += "HAVING Casas >= 18 ";
        query += "ORDER BY Casas ASC";
        jdbcutil = JDBCUtilities()
        conexion = jdbcutil.getConnection()
        cursor = conexion.execute(query)
        for fila in cursor:
            objProyecto: Proyecto = Proyecto()
            objProyecto.setNum_casas(fila[0])
            objProyecto.setNombre_constructora(fila[1])
            arrayProyectos.append(objProyecto)
        conexion.close()
        return arrayProyectos